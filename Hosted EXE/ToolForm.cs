﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Hosted_EXE
{
    public partial class ToolForm : Form
    {

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongA", SetLastError = true)]
        private static extern long GetWindowLong(IntPtr hwnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLongA", SetLastError = true)]
        private static extern long SetWindowLong(IntPtr hwnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern long SetWindowPos(IntPtr hwnd, long hWndInsertAfter, long x, long y, long cx, long cy, long wFlags);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);
		

        public ToolForm()
        {
            InitializeComponent();
            HostTestApplication();
        }

        public void HostTestApplication()
        {

            // Start the process
            IntPtr appWin = IntPtr.Zero;
            Process p = System.Diagnostics.Process.Start("notepad");

            // Wait for process to be created and enter idle condition
            p.WaitForInputIdle();
            p.EnableRaisingEvents = true;
            p.Exited += (s, ee) => Console.WriteLine("Process Exited!");
            while (p.Handle == IntPtr.Zero)
            {
                System.Threading.Thread.Sleep(100);
                p.Refresh();
            }

            // Get the main handle
            appWin = p.MainWindowHandle;

            // Put it into this form
            SetParent(appWin, panel1.Handle);

            // Move the window to overlay it on this window
            MoveWindow(appWin, -2, -22, this.Width, this.Height, true);

            // Resize window when panel size changed
            panel1.Resize += (s, e) => MoveWindow(appWin, -2, -22, this.Width, this.Height, true);

        }
    }
}
