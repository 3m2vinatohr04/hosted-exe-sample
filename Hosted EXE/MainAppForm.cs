﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hosted_EXE
{
    public partial class MainAppForm : Form
    {
        public MainAppForm()
        {
            InitializeComponent();
            // Set additional form properties
            this.IsMdiContainer = true;
            ToolForm tf = new ToolForm();
            tf.Show();
            tf.MdiParent = this; // hosted notepad.exe would just terminate here
        }
    }
}
